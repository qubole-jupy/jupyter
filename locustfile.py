"""
Runs the performance test for Jupyter in QDS using Locust
"""
import json
import uuid
import random
import string
import base64
from locust import HttpLocust, TaskSequence, seq_task

# pylint: disable=too-few-public-methods


class JupyterlabTaskSequence(TaskSequence):

    """
    This class contains all the tasks that needs to be performed as part of running
    performance tests for Jupyter in QDS
    """
    def __init__(self, parent):
        """
        Initializes the token, headers and cookie params
        """
        super(JupyterlabTaskSequence, self).__init__(parent)
        self.token = "AUTH_TOKEN"
        self.headers = {}
        self.cookies = ""
        self.uuid = None
        self.notebook_name = None

    def login(self):
        """
        Logs into jupyter env or starts asterix
        """
        headers = {'X-Auth-Token': self.token, 'Content-Type': 'application/json',
                   'Accept': 'application/json'}
        response = self.client.get("lab/workspaces/" + "QBOL_USER", headers=headers, name="login")
        return response

    def on_start(self):
        """ on_start is called when a Locust start before any task is scheduled """
        self.uuid = str(uuid.uuid4().hex)
        session_response = self.login()
        cookie_value = session_response.cookies.get('_xsrf')
        # self.cookies = "_xsrf=" + cookie_value
        self.cookies = dict(_xsrf=cookie_value)
        self.headers = {
            'X-Auth-Token': self.token,
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'X-XSRFToken': cookie_value}

    @seq_task(1)
    def create(self):
        """
        This task creates a jupyter notebook
        """
        name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))
        name = "test_" + name
        data = json.dumps({"type": "notebook", "path": "NB_PATH",
                           "kernel": {"name": "pysparkkernel"}, "name": name})
        res = self.client.post("api/contents/NB_PATH",
                               data=data,
                               headers=self.headers,
                               name="create",
                               cookies=self.cookies)
        assert res.status_code == 201
        self.notebook_name = res.json()['name']

    @seq_task(2)
    def read(self):
        """
        This task reads the notebook
        """
        url = "api/contents/NB_PATH/%s?type=notebook&content=1" % \
              self.notebook_name
        res = self.client.get(url, name="read", headers=self.headers, cookies=self.cookies)
        assert res.status_code == 200

    @seq_task(3)
    def update(self):
        """
        This task updates the notebook contents
        """
        url = "api/contents/NB_PATH/%s" % self.notebook_name
        with open('sample.ipynb') as samp:
            data = samp.read()
        encoded = base64.b64encode(data)
        data = json.dumps({"name": self.notebook_name,
                           "content": encoded,
                           "path": self.notebook_name,
                           "format": "base64",
                           "type": "file"})
        res = self.client.put(url, data=data, headers=self.headers, name="update",
                              cookies=self.cookies)
        assert res.status_code == 200

    @seq_task(4)
    def delete(self):
        """
        This task deletes the notebook
        """
        url = "api/contents/NB_PATH/%s" % self.notebook_name
        res = self.client.delete(url, name="delete", headers=self.headers, cookies=self.cookies)
        assert res.status_code == 204


class JupyterlabLocust(HttpLocust):
    """
    This is the locust class for running Jupyterlab performance tests
    """
    host = "https://" + "ENV_URL" + "/jupyter-notebook-" + "CLUSTER_ID" + "/"
    task_set = JupyterlabTaskSequence
    min_wait = 1000
    max_wait = 1000
